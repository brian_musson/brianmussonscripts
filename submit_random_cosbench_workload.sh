#!/bin/bash

# Bucket naming
BUCKET_PREFIX="testbucket-"
BUCKET_SUFFIX="$(uuidgen)"

# Array of sizes to choose from, in KB
SIZES=( 4096 2048 10240 1024 1 2 100 512 )

# Number of objects to create
OBJECTS="6000"

# Paths to important files
CLI_PATH="/home/cosbench/cos/cli.sh"
TMP_PATH="/home/cosbench/cos/scality/tmp/"


# S3 access information
ACCESS_KEY="cfservices"
SECRET_KEY="secretkey"
S3ENDPOINT="http://euwstore.gecis.io"

##### End of Configuration #####

# Make the tmp directory
mkdir -p "$TMP_PATH"

# Choose a size to run
SIZE="${SIZES[$RANDOM % ${#SIZES[*]}]}"

#give our workload file a description
NAME="random r, w, rw test using $OBJECTS $SIZE KB sized objects"

cat > "$TMP_PATH"workload-"$BUCKET_SUFFIX".xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<workload name="$NAME"
    description="$NAME" config="">
    <auth type="none"/>
    <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
    <workflow config="">
        <workstage name="init" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="init" type="init" workers="1" interval="5"
                division="container" runtime="0" rampup="0" rampdown="0"
                afr="0" totalOps="1" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1)">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="init" ratio="100" division="container"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(0,0);sizes=c(0)B;cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1)" id="op1"/>
            </work>
        </workstage>
        <workstage name="prepare" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="prepare" type="prepare" workers="25"
                interval="5" division="object" runtime="0" rampup="0"
                rampdown="0" afr="0" totalOps="25" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="prepare" ratio="100" division="object"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB" id="op1"/>
            </work>
        </workstage>
        <workstage name="Read 20 workers" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="main" type="normal" workers="20" interval="5"
                division="none" runtime="180" rampup="0" rampdown="0"
                afr="$OBJECTS0" totalOps="0" totalBytes="0" config="">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="read" ratio="100" division="none"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB" id="op1"/>
            </work>
        </workstage>
        <workstage name="Write 20 workers" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="main" type="normal" workers="40" interval="5"
                division="none" runtime="180" rampup="0" rampdown="0"
                afr="$OBJECTS0" totalOps="0" totalBytes="0" config="">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="write" ratio="100" division="none"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=write-object;objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB" id="op1"/>
            </work>
        </workstage>
        <workstage name="read/write 20 workers" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="main" type="normal" workers="50" interval="5"
                division="none" runtime="180" rampup="0" rampdown="0"
                afr="$OBJECTS0" totalOps="0" totalBytes="0" config="">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="read" ratio="50" division="none"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB" id="op1"/>
                <operation type="write" ratio="50" division="none"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=rw-object;objects=r(1,$OBJECTS);sizes=r($SIZE,$SIZE)KB" id="op2"/>
            </work>
        </workstage>
        <workstage name="cleanup-read" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="cleanup" type="cleanup" workers="30"
                interval="5" division="object" runtime="0" rampup="0"
                rampdown="0" afr="0" totalOps="50" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS)">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="cleanup" ratio="100" division="object"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(1,$OBJECTS);deleteContainer=false" id="op1"/>
            </work>
        </workstage>
        <workstage name="cleanup-write" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="cleanup" type="cleanup" workers="30"
                interval="5" division="object" runtime="0" rampup="0"
                rampdown="0" afr="0" totalOps="50" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=write-object;objects=r(1,$OBJECTS)">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="cleanup" ratio="100" division="object"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=write-object;objects=r(1,$OBJECTS);deleteContainer=false" id="op1"/>
            </work>
        </workstage>
        <workstage name="cleanup-rw" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="cleanup" type="cleanup" workers="30"
                interval="5" division="object" runtime="0" rampup="0"
                rampdown="0" afr="0" totalOps="50" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=rw-object;objects=r(1,$OBJECTS)">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="cleanup" ratio="100" division="object"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);oprefix=rw-object;objects=r(1,$OBJECTS);deleteContainer=false" id="op1"/>
            </work>
        </workstage>
        <workstage name="dispose" closuredelay="0" config="">
            <auth type="none"/>
            <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
            <work name="dispose" type="dispose" workers="1" interval="5"
                division="container" runtime="0" rampup="0" rampdown="0"
                afr="0" totalOps="1" totalBytes="0" config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1)">
                <auth type="none"/>
                <storage type="s3" config="accesskey=$ACCESS_KEY;secretkey=$SECRET_KEY;endpoint=$S3ENDPOINT"/>
                <operation type="dispose" ratio="100"
                    division="container"
                    config="cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1);objects=r(0,0);sizes=c(0)B;cprefix=$BUCKET_PREFIX$BUCKET_SUFFIX;containers=r(1,1)" id="op1"/>
            </work>
        </workstage>
    </workflow>
</workload>
EOF


# Submit the workload
exec "$CLI_PATH" submit "$TMP_PATH"workload-"$BUCKET_SUFFIX".xml 
